<?php

namespace App\Form;

use App\Entity\Produits;
use App\Entity\CategoriesProd;                      // add: relation type
use Symfony\Bridge\Doctrine\Form\Type\EntityType;   // add: relation type
// use Symfony\Bridge\Doctrine\Form\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProduitsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nom')
            // ->add('imgFile')
            ->add('imgFile', FileType::class, [
                'mapped' => false
            ])            
            ->add('prix')
            ->add('quantite')
            ->add('description')                            
            //->add('CategoriesProd')                       // Prob Cat est une relation 
            ->add('CategoriesProd', EntityType::class, [    // CategoriesProd
                'label' => 'Catégorie',
                'class' => CategoriesProd::class,           // CategoriesProd
                'choice_label' => 'nom',                    // nom
                'attr' => [
                    'class' => 'form-control mb-2',
                    'required' => true,                     // true / false
                ]])
            
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Produits::class,
        ]);
    }
}
