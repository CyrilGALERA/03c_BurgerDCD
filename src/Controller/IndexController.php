<?php

namespace App\Controller;

use App\Entity\Produits;                // Entity donnée utilisée
use App\Repository\ProduitsRepository;  // EntityRepo donnée utilisée

use App\Entity\CategoriesProd;          // Entity donnée utilisée
use App\Repository\CategoriesProdRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    
    // public function index(): Response
    // {
    //     return $this->render('index/index.html.twig', [
    //         'controller_name' => 'IndexController',
    //     ]);
    // }
    
    // === Juste Produits
    // public function index(ProduitsRepository $produitsRepository): Response
    // {
    //     return $this->render('index/index.html.twig', [
    //         'produits' => $produitsRepository->findAll(),
    //     ]);
    // }

    public function index(ProduitsRepository $produitsRepository, CategoriesProdRepository $categoriesProdRepository): Response
    {
        return $this->render('index/index.html.twig', [
            'produits' => $produitsRepository->findAll(),
            'categories_prod' => $categoriesProdRepository->findAll(),
        ]);
    }
}
