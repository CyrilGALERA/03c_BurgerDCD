<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class V1CommanderController extends AbstractController
{
    #[Route('/v1/commander', name: 'app_v1_commander')]
    public function index(): Response
    {
        return $this->render('v1_commander/index.html.twig', [
            'controller_name' => 'V1CommanderController',
        ]);
    }
}
