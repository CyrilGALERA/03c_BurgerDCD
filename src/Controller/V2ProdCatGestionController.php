<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class V2ProdCatGestionController extends AbstractController
{
    #[Route('/v2/prod/cat/gestion', name: 'app_v2_prod_cat_gestion')]
    public function index(): Response
    {
        return $this->render('v2_prod_cat_gestion/index.html.twig', [
            'controller_name' => 'V2ProdCatGestionController',
        ]);
    }
}
