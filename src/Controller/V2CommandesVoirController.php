<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class V2CommandesVoirController extends AbstractController
{
    #[Route('/v2/commandes/voir', name: 'app_v2_commandes_voir')]
    public function index(): Response
    {
        return $this->render('v2_commandes_voir/index.html.twig', [
            'controller_name' => 'V2CommandesVoirController',
        ]);
    }
}
