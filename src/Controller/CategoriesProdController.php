<?php

namespace App\Controller;

use App\Entity\CategoriesProd;
use App\Form\CategoriesProdType;
use App\Repository\CategoriesProdRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/categories/prod')]
class CategoriesProdController extends AbstractController
{
    #[Route('/', name: 'app_categories_prod_index', methods: ['GET'])]
    public function index(CategoriesProdRepository $categoriesProdRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');    // DCD: control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        return $this->render('categories_prod/index.html.twig', [
            'categories_prods' => $categoriesProdRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_categories_prod_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CategoriesProdRepository $categoriesProdRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');    // DCD: control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        $categoriesProd = new CategoriesProd();
        $form = $this->createForm(CategoriesProdType::class, $categoriesProd);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoriesProdRepository->add($categoriesProd);
            return $this->redirectToRoute('app_categories_prod_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('categories_prod/new.html.twig', [
            'categories_prod' => $categoriesProd,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_categories_prod_show', methods: ['GET'])]
    public function show(CategoriesProd $categoriesProd): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');    // DCD: control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        return $this->render('categories_prod/show.html.twig', [
            'categories_prod' => $categoriesProd,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_categories_prod_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, CategoriesProd $categoriesProd, CategoriesProdRepository $categoriesProdRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');    // DCD: control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        $form = $this->createForm(CategoriesProdType::class, $categoriesProd);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoriesProdRepository->add($categoriesProd);
            return $this->redirectToRoute('app_categories_prod_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('categories_prod/edit.html.twig', [
            'categories_prod' => $categoriesProd,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_categories_prod_delete', methods: ['POST'])]
    public function delete(Request $request, CategoriesProd $categoriesProd, CategoriesProdRepository $categoriesProdRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');    // DCD: control du user (registred = "admin" = [] 'ROLE_USER par defaut)
        if ($this->isCsrfTokenValid('delete'.$categoriesProd->getId(), $request->request->get('_token'))) {
            $categoriesProdRepository->remove($categoriesProd);
        }

        return $this->redirectToRoute('app_categories_prod_index', [], Response::HTTP_SEE_OTHER);
    }
}
