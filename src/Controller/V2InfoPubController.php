<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class V2InfoPubController extends AbstractController
{
    #[Route('/v2/info/pub', name: 'app_v2_info_pub')]
    public function index(): Response
    {
        return $this->render('v2_info_pub/index.html.twig', [
            'controller_name' => 'V2InfoPubController',
        ]);
    }
}
