<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class V2PlanSiteAdminController extends AbstractController
{
    #[Route('/v2/plan/site/admin', name: 'app_v2_plan_site_admin')]
    public function index(): Response
    {
        return $this->render('v2_plan_site_admin/index.html.twig', [
            'controller_name' => 'V2PlanSiteAdminController',
        ]);
    }
}
