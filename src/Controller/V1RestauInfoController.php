<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class V1RestauInfoController extends AbstractController
{
    #[Route('/v1/restau/info', name: 'app_v1_restau_info')]
    public function index(): Response
    {
        return $this->render('v1_restau_info/index.html.twig', [
            'controller_name' => 'V1RestauInfoController',
        ]);
    }
}
